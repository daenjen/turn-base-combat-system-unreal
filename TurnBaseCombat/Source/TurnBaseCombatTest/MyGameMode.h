// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MyGameMode.generated.h"

/**
 *
 */
UCLASS()
class TURNBASECOMBATTEST_API AMyGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AMyGameMode();

public:
	//HUD states
	enum EHUDState : uint8
	{
		HS_InGame,
		HS_Inventory,
		HS_ShopGeneral,
		HP_ShopWeapon
	};

public:
	uint8 GetHUDState();

	virtual void BeginPlay() override;

	void ApplyHUDChange();

	UFUNCTION(BlueprintCallable, Category = "HUD Functions")
		void ChangeHUDState(uint8 NewState);

	bool ApplyHUD(TSubclassOf<class UUserWidget> WidgetToApply, bool bShowMouseCursor, bool EnableClickEvents);

protected:
	uint8 HUDState;

	//HUD ingame
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUDWidgets", Meta = (BlueprintProtected = "true"))
		TSubclassOf<class UUserWidget> IngameHUDClass;
	//HUD inventory
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUDWidgets", Meta = (BlueprintProtected = "true"))
		TSubclassOf<class UUserWidget> InventoryHUDClass;
	//HUD shop
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUDWidgets", Meta = (BlueprintProtected = "true"))
		TSubclassOf<class UUserWidget> ShopGeneralHUDClass;

	UPROPERTY()
		class UUserWidget* CurrentWidget;
};

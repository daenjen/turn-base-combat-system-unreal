// Fill out your copyright notice in the Description page of Project Settings.

#include "MyGameMode.h"
#include "InventoryHUD.h"
#include "MyCharacter.h"
#include "Blueprint/UserWidget.h"
#include "Kismet/GameplayStatics.h"
#include "UObject/ConstructorHelpers.h"

void AMyGameMode::BeginPlay()
{
	ApplyHUDChange();
}

AMyGameMode::AMyGameMode() : Super()
{
	//Set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprint/Character/BP_NewCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	//Use our custom HUD class
	HUDClass = AInventoryHUD::StaticClass();

	//Set as default HUD
	HUDState = EHUDState::HS_InGame;
}

void AMyGameMode::ApplyHUDChange()
{
	//Remove previous HUD
	if (CurrentWidget != nullptr)
	{
		CurrentWidget->RemoveFromParent();
	}

	//HUD states
	switch (HUDState)
	{
		case EHUDState::HS_InGame:
		{
			ApplyHUD(IngameHUDClass, false, false);
			break;
		}
		case EHUDState::HS_Inventory:
		{
			ApplyHUD(InventoryHUDClass, true, true);
			break;
		}
		case EHUDState::HS_ShopGeneral:
		{
			ApplyHUD(ShopGeneralHUDClass, true, true);
			break;
		}
		default:
		{
			ApplyHUD(IngameHUDClass, false, false);
			break;
		}
	}
}

uint8 AMyGameMode::GetHUDState()
{
	return HUDState;
}

void AMyGameMode::ChangeHUDState(uint8 NewState)
{
	HUDState = NewState;
	ApplyHUDChange();
}

bool AMyGameMode::ApplyHUD(TSubclassOf<class UUserWidget> WidgetToApply, bool bShowMouseCursor, bool EnableClickEvents)
{
	//Reference of the player 
	AMyCharacter* myCharacter = Cast<AMyCharacter>(UGameplayStatics::GetPlayerCharacter(this,0));
	APlayerController* myController = GetWorld()->GetFirstPlayerController();

	if (WidgetToApply != nullptr)
	{
		//Enable mouse 
		myController->bShowMouseCursor = bShowMouseCursor;
		myController->bEnableClickEvents = EnableClickEvents;

		//Create widget
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), WidgetToApply);

		if (CurrentWidget != nullptr)
		{
			//Show widget
			CurrentWidget->AddToViewport();
			return true;
		}
		else
			return false;
	}
	else
		return false;
}

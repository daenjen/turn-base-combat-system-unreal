// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Object_Interactable.h"
#include "Object_PickUp.h"
#include "Engine/TriggerBox.h"
#include "MyCharacter.generated.h"

UCLASS()
class TURNBASECOMBATTEST_API AMyCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* FollowCamera;

	virtual void BeginPlay();

public:
	// Sets default values for this character's properties
	AMyCharacter();
	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
		float BaseLookUpRate;

	//Player current money
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		int playerMoney;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		FString helpText;

	//Player Inventory
	UPROPERTY(EditAnywhere)
		TArray<AObject_PickUp*> inventory;

public:
	//Overlap Event start
	UFUNCTION()
		void OnOverlapBegin(class AActor* OverlappedActor, class AActor* OtherActor);

	//Overlap Event end
	UFUNCTION()
		void OnOverlapEnd(class AActor* OverlappedActor, class AActor* OtherActor);

	//Use item in the inventory
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void UseItemInventorySlot(int32 itemSlot);

public:
	//Check if add item to inventory
	UFUNCTION(BlueprintPure, Category = "Inventory")
		bool addItemInventory(AObject_PickUp* item);

	//Get image of the item in inventory
	UFUNCTION(BlueprintPure, Category = "Inventory")
		UTexture2D* GetImageInventoryItemSlot(int32 itemSlot);

	//Get name of the item in inventory
	UFUNCTION(BlueprintPure, Category = "Inventory")
		FString GetNameInventoryItemSlot(int32 itemSlot);

protected:

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/**
	 * Called via input to turn at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate.
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	//References
	APlayerController* myController;
	AObject_Interactable* CurrentInteractable;

public:
	//Open inventory
	void Inventory();

	//Interact with items
	void Interact();

private:
	//Checks
	bool canInteract;
	bool mouseInteract;

public:
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

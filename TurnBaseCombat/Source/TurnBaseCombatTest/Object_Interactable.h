// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Object_Interactable.generated.h"

UCLASS()
class TURNBASECOMBATTEST_API AObject_Interactable : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AObject_Interactable();

public:	

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Interact_Implementation();

	//All interactable will have static mesh
	UPROPERTY(EditAnywhere, Category = "Interactable Properties")
		class UStaticMeshComponent* InteractableMesh;

	//All interactable will have a trigger box
	UPROPERTY(EditAnywhere, Category = "Interactable Properties")
		class UBoxComponent* TriggerBox;

	//All interactable will have text
	UPROPERTY(EditAnywhere, Category = "Interactable Properties")
		FString InteractableText;

};

// Fill out your copyright notice in the Description page of Project Settings.


#include "Object_Interactable.h"

// Sets default values
AObject_Interactable::AObject_Interactable()
{
	//Help text
	InteractableText = FString("Press E to pick up");
}

// Called when the game starts or when spawned
void AObject_Interactable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AObject_Interactable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AObject_Interactable::Interact_Implementation()
{
	GLog->Log("Interact base class: Interact() YOU SHOULD NOT BE SEEING THIS");
}


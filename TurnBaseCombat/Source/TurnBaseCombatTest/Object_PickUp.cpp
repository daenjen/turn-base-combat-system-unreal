// Fill out your copyright notice in the Description page of Project Settings.


#include "Object_PickUp.h"
#include "MyCharacter.h"
#include "Components/BoxComponent.h"
#include "Kismet/GameplayStatics.h"

AObject_PickUp::AObject_PickUp()
{
	//set item pick up information
	InteractableMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PickupMesh"));
	InteractableMesh->SetSimulatePhysics(true);

	//Trigger box
	TriggerBox = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerBox"));
	TriggerBox->SetBoxExtent(FVector(75.f,75.f,75.f));

	//Item texts
	ItemName = FString("Enter an item name");
	InteractableText = FString("Press E to pickup");
	Value = 0;
}

void AObject_PickUp::BeginPlay()
{
	//Set text
	InteractableText = FString::Printf(TEXT("%s: Press E to pick up"), *ItemName);
}

void AObject_PickUp::Interact_Implementation()
{
	//Get the player 
	AMyCharacter* player = Cast<AMyCharacter>(UGameplayStatics::GetPlayerCharacter(this, 0));

	//Pick up items
	if (player->addItemInventory(this))
	{
		OnPickedUp();
	}
}

void AObject_PickUp::OnPickedUp()
{
	//Reset mesh on pick up
	InteractableMesh->SetVisibility(false);
	InteractableMesh->SetSimulatePhysics(false);
	InteractableMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	//Reset box trigger on pick up
	TriggerBox->SetVisibility(false);
	TriggerBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void AObject_PickUp::Use_Implementation()
{
	GLog->Log("Use() from base pickup class: YOU SHOULD NOT BE SEEING THIS");
}


// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Object_Interactable.h"
#include "Object_PickUp.generated.h"

/**
 * 
 */
UCLASS()
class TURNBASECOMBATTEST_API AObject_PickUp : public AObject_Interactable
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, Category = " Pickup Properties")
		UTexture2D* pickupImage;

	UPROPERTY(EditAnywhere, Category = " Pickup Properties")
		FString ItemName;

	UPROPERTY(EditAnywhere, Category = " Pickup Properties")
		int32 Value;

	UPROPERTY(EditAnywhere, Category = "Reference")
		ACharacter* MyPlayer;

public:
	AObject_PickUp();

	virtual void BeginPlay() override;

	virtual void Interact_Implementation() override;

	virtual void Use_Implementation();

	void OnPickedUp();
};
